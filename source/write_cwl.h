#ifndef DEF_write_cwl
#define DEF_write_cwl

#include "IncludeDefine.h"
#include "Parameters.h"

void WriteCwl(ostream* logStdOut, const Parameters& params);

#endif DEF_write_cwl
